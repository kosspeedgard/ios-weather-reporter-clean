//
//  URLSuffixs.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct URLSuffixs {
    //MARK: Weather
    struct Weather {
        static let weather = "data/2.5/weather?"
        static let forecast = "data/2.5/forecast?"
    }
}
