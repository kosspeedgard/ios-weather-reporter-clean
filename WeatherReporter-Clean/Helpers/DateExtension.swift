//
//  DateExtension.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

extension Date {
    init(unix:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(unix))
    }
    
    func string(format: String) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}
