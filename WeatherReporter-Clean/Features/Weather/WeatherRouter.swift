//
//  WeatherRouter.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import UIKit

class WeatherRouter: NSObject {
    let viewController: WeatherViewController
    
    init(viewController: WeatherViewController) {
        self.viewController = viewController
    }
}

extension WeatherRouter: WeatherRoutable {
    
}
