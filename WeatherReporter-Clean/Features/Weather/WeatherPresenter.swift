//
//  WeatherPresenter.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class WeatherPresenter: NSObject {
    let displayable: WeatherDisplayable
    
    init(displayable: WeatherDisplayable) {
        self.displayable = displayable
    }
}

//MARK: WeatherPresentable
extension WeatherPresenter: WeatherPresentable {
    func presentWeatherInformationSuccess(response: Weather.WeatherSection.Response) {
        let viewModel = weather(response: response)
        displayable.displayWeatherInformation(viewModel: viewModel)
    }
    
    func presentForecastInformationSuccess(response: Weather.ForecastSection.Response) {
        let viewModel = forecasts(response: response)
        displayable.displayForecastInformation(viewModel: viewModel)
    }
    
    func presentInformationFailure() {
        displayable.display(title: "Failure", message: "Something worng.", action: { [weak self] (action) in
            self?.displayable.displayedAlert()
        })
    }
}

//MARK: Handle Response
fileprivate extension WeatherPresenter {
    func weather(response: Weather.WeatherSection.Response) -> Weather.WeatherSection.ViewModel {
        let temperature = response.isCelsius ? Utils.covertToCelsius(kelvin: response.weather.main?.temp ?? 0.0) : Utils.convertToFarenheit(kelvin: response.weather.main?.temp ?? 0.0)
        
        let formattedTemperature = "\(temperature)°"
        let humidity = "\(response.weather.main?.humidity ?? 0)%"
        let wind = "\(response.weather.wind?.speed ?? 0) km/hr"
        let cityName = response.weather.name ?? ""
        let temperatureType = response.isCelsius ? kToF : kToC
        
        let weather = Weather.WeatherSection.WeatherViewModel(temperature: formattedTemperature,
                                                                                humidity: humidity,
                                                                                wind: wind,
                                                                                cityName: cityName,
                                                                                temperatureType: temperatureType)
        return Weather.WeatherSection.ViewModel(weather: weather)
    }
    
    func forecasts(response: Weather.ForecastSection.Response) -> Weather.ForecastSection.ViewModel {
        var forecasts = [Weather.ForecastSection.ForecastViewModel]()
        
        for i in 0..<response.forecasts.count - 1 {
            let forecast = response.forecasts[i]
            
            let temperature = response.isCelsius ? Utils.covertToCelsius(kelvin: forecast.main?.temperature ?? 0.0) : Utils.convertToFarenheit(kelvin: forecast.main?.temperature ?? 0.0)
            let day = Utils.timeFromUtils(time: forecast.dateTime ?? 0, format: kShortTime)
            let description = forecast.weather?.first?.description ?? ""
            let humidity = "\(forecast.main?.humidity ?? 0)%"
            let wind = "\(forecast.wind?.speed ?? 0) km/hr"
            let formattedTemperature = "\(temperature)°"
            
            let _forecast = Weather.ForecastSection.ForecastViewModel(temperature: formattedTemperature,
                                                                     humidity: humidity,
                                                                     wind: wind,
                                                                     day: day ?? "",
                                                                     description: description)
            
            forecasts.append(_forecast)
        }
        
        return Weather.ForecastSection.ViewModel(forecasts: forecasts)
    }
}
