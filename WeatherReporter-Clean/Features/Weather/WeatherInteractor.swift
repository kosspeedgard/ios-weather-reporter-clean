//
//  WeatherInteractor.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class WeatherInteractor: NSObject {
    let presenter: WeatherPresentable
    var worker: WeatherWorker!
    
    init(presenter: WeatherPresentable) {
        self.presenter = presenter
        self.worker = WeatherWorker()
    }
}

//MARK: WeatherBusinessLogic
extension WeatherInteractor: WeatherBusinessLogic {
    func fetchWeatherInformation(request: Weather.WeatherSection.Request){
        worker.fetchWeather(city: request.city, completion: { (weatherBase) in
            guard let weather = weatherBase else {
                self.presenter.presentInformationFailure()
                return
            }
            
            let response = Weather.WeatherSection.Response(weather: weather, isCelsius: request.isCelsius)
            self.presenter.presentWeatherInformationSuccess(response: response)
        }, failure: { [weak self] (error) in
            self?.presenter.presentInformationFailure()
        })
    }
    
    func fetchForecastInformation(request: Weather.ForecastSection.Request) {
        worker.fetchForecast(city: request.city, completion: { [weak self] (forecastBase) in
            guard let forecasts = forecastBase?.forecasts else {
                self?.presenter.presentInformationFailure()
                return
            }
            
            let response = Weather.ForecastSection.Response(forecasts: forecasts, isCelsius: request.isCelsius)
            self?.presenter.presentForecastInformationSuccess(response: response)
        }, failure: { [weak self] (error) in
            self?.presenter.presentInformationFailure()
        })
    }
}
