//
//  WeatherView.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import UIKit

class WeatherView: UIView {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var convertTypeButton: UIButton!
    
    //Constraint
    @IBOutlet weak var searchButtonWidthConstraint: NSLayoutConstraint!
    
    private var searchButtonWidth: CGFloat!
    
    var isCelsius: Bool = true
    var city: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setup() {
        tableView.backgroundColor = .clear
        tableView.layer.borderColor = UIColor.white.cgColor
        tableView.layer.borderWidth = 0.5
        tableView.layer.cornerRadius = 8
        
        convertTypeButton.layer.borderColor = UIColor.black.cgColor
        convertTypeButton.layer.borderWidth = 1
        convertTypeButton.layer.cornerRadius = convertTypeButton.bounds.size.height / 2
        
        searchTextField.keyboardType = .asciiCapable
        
        searchButtonWidth = searchButtonWidthConstraint.constant
        
        setTemperatureTypeTitle()
        
        city = kCity
    }
}

//MARK: Bind
extension WeatherView {
    func weatherBind(viewModel: Weather.WeatherSection.ViewModel) {
        cityLabel.text = viewModel.weather.cityName
        temperatureLabel.text = viewModel.weather.temperature
        humidityLabel.text = viewModel.weather.humidity
        windLabel.text = viewModel.weather.wind
        convertTypeButton.setTitle(viewModel.weather.temperatureType, for: .normal)
    }
}

//MARK: Datasource, Delegate
extension WeatherView {
    func setSource(source: WeatherViewController) {
        tableView.delegate = source
        tableView.dataSource = source
        
        searchTextField.delegate = source
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
}

//MARK: View
extension WeatherView {
    func reloadData() {
        tableView.reloadData()
    }
    
    func endEdit() {
        endEditing(true)
    }
    
    func clear() {
        city = kCity
        searchTextField.text = nil
    }
}

//MARK: SearchButton
extension WeatherView {
    func visibleSearchButton() {
        if let text = searchTextField.text, !text.isEmpty {
            hiddenSearchButton(constant: 60)
        }
        else {
            hiddenSearchButton(constant: 0)
        }
    }
    
    fileprivate func hiddenSearchButton(constant: CGFloat) {
        searchButtonWidth = constant
        
        UIView.animate(withDuration: 0.1, animations: { [unowned self] in
            self.searchButtonWidthConstraint.constant = constant
            self.layoutIfNeeded()
        })
    }
}

//MARK: Temperature
extension WeatherView {
    func convertTemperatureType() {
        isCelsius = !isCelsius
        
        setTemperatureTypeTitle()
    }
    
    fileprivate func setTemperatureTypeTitle() {
        let temperatureType = isCelsius ? kToC : kToF
        convertTypeButton.setTitle(temperatureType, for: .normal)
    }
}

//MARK: Target
extension WeatherView {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, text.isEmpty {
            city = kCity
        }
        else {
            city = textField.text
        }
        
        visibleSearchButton()
    }
}
