//
//  WeatherViewController.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    @IBOutlet weak var weatherView: WeatherView!
    
    //MARK: VIP variables
    var interactor: WeatherBusinessLogic!
    var router: WeatherRoutable!
    
    //MARK: ViewModel
    var forecastViewModel: Weather.ForecastSection.ViewModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configuration()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTappedSearch(_ sender: Any) {
        loadWeather()
        loadForecast()
    }
    
    @IBAction func onTappedConvertType(_ sender: Any) {
        weatherView.convertTemperatureType()
        
        loadWeather()
        loadForecast()
    }
    
    @IBAction func onTappedAtScreen(_ sender: Any) {
        weatherView.endEdit()
    }
}

//MARK: Setup
fileprivate extension WeatherViewController {
    func setup() {
        loadWeather()
        loadForecast()
    }
    
    func setupViews() {
        weatherView.setSource(source: self)
    }
    
    func configuration() {
        WeatherConfigurator.shared.configure(viewController: self)
    }
    
    func loadWeather() {
        let request = Weather.WeatherSection.Request(city: weatherView.city, isCelsius: weatherView.isCelsius)
        interactor.fetchWeatherInformation(request: request)
    }
    
    func loadForecast() {
        let request = Weather.ForecastSection.Request(city: weatherView.city, isCelsius: weatherView.isCelsius)
        interactor.fetchForecastInformation(request: request)
    }
}

//MARK: UITableViewDelegate, UITableViewDataSource
extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastViewModel?.forecasts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kForecastTableViewCell, for: indexPath) as! ForecastTableViewCell
        let forecast = forecastViewModel?.forecasts[indexPath.row]
        cell.bind(forecast: forecast)
        
        return cell
    }
}

//MARK: UITextFieldDelegate
extension WeatherViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        weatherView.visibleSearchButton()
    }
}

//MARK: WeatherViewControllerInput
extension WeatherViewController: WeatherDisplayable {
    func displayWeatherInformation(viewModel: Weather.WeatherSection.ViewModel) {
        weatherView.weatherBind(viewModel: viewModel)
    }
    
    func displayForecastInformation(viewModel: Weather.ForecastSection.ViewModel) {
        forecastViewModel = viewModel
        weatherView.reloadData()
    }
    
    func displayedAlert() {
        weatherView.clear()
    }
}
