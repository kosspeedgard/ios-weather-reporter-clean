//
//  WeatherConfigurator.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import UIKit

class WeatherConfigurator: NSObject {
    static let shared: WeatherConfigurator = WeatherConfigurator()
    
    private override init() {}
    
    func configure(viewController: WeatherViewController) {
        let router = WeatherRouter(viewController: viewController)
        let presenter = WeatherPresenter(displayable: viewController)
        let interactor = WeatherInteractor(presenter: presenter)
        
        viewController.interactor = interactor
        viewController.router = router
    }
}

extension WeatherViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

