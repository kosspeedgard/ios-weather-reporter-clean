//
//  WeatherWorker.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class WeatherWorker: BaseWorker {
    func fetchWeather(city: String?, completion: @escaping(_ weatherBase: GraphWeatherBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        super.sendRequestWith(method: .GET, host: .baseURL, urlSuffixs: URLSuffixs.Weather.weather + "q=\(city ?? "bangkok")", json: nil, responseType: GraphWeatherBase.self, completion: { (weatherBase) in
            completion(weatherBase)
        }, failure: { (error) in
            failure(error)
        })
    }
    
    func fetchForecast(city: String?, completion: @escaping(_ forecastBase: GraphForecastBase?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        super.sendRequestWith(method: .GET, host: .baseURL, urlSuffixs: URLSuffixs.Weather.forecast + "q=\(city ?? "bangkok")&cnt=7", json: nil, responseType: GraphForecastBase.self, completion: { (forecastBase) in
            completion(forecastBase)
        }, failure: { (error) in
            failure(error)
        })
    }
    
//    func covertToCelsius(kelvin: Double) -> Int {
//        let celsius = kelvin - 273.15
//        return Int(celsius)
//    }
//
//    func convertToFarenheit(kelvin: Double) -> Int {
//        let farenheit = 9 / 5 * (kelvin - 273.15) + 32
//        return Int(farenheit)
//    }
}
