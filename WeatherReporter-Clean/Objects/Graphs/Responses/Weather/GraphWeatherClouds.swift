//
//  GraphWeatherClouds.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphWeatherClouds: GraphDecoder {
    var all: Double?
    
    init(json: JSON?) {
        self.all = json?["all"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
