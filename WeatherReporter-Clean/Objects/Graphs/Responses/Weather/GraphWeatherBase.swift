//
//  GraphWeatherBase.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphWeatherBase: GraphDecoder {
    var coordinate: GraphWeatherCoordinate?
    var weather: GraphWeather?
    var base: String?
    var main: GraphWeatherMain?
    var visibility: Int?
    var wind: GraphWeatherWind?
    var clouds: GraphWeatherClouds?
    var dt: Int?
    var system: GraphWeatherSystem?
    var id: Int?
    var name: String?
    var code: Int?
    
    init(json: JSON?) {
        self.coordinate = GraphWeatherCoordinate(json: json?["coord"] as? JSON)
        self.weather = GraphWeather(json: json?["weather"] as? JSON)
        self.base = json?["base"] as? String
        self.main = GraphWeatherMain(json: json?["main"] as? JSON)
        self.visibility = json?["visibility"] as? Int
        self.wind = GraphWeatherWind(json: json?["wind"] as? JSON)
        self.clouds = GraphWeatherClouds(json: json?["clouds"] as? JSON)
        self.dt = json?["dt"] as? Int
        self.system = GraphWeatherSystem(json: json?["system"] as? JSON)
        self.id = json?["id"] as? Int
        self.name = json?["name"] as? String
        self.code = json?["code"] as? Int
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
