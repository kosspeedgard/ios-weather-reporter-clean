//
//  GraphWeatherMain.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphWeatherMain: GraphDecoder {
    var temp: Double?
    var pressure: Double?
    var humidity: Double?
    var tempMin: Double?
    var tempmax: Double?
    
    init(json: JSON?) {
        self.temp = json?["temp"] as? Double
        self.pressure = json?["pressure"] as? Double
        self.humidity = json?["humidity"] as? Double
        self.tempMin = json?["temp_min"] as? Double
        self.tempmax = json?["temp_max"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
