//
//  GraphWeatherSystem.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphWeatherSystem: GraphDecoder {
    var type: Int?
    var id: Int?
    var message: Double?
    var country: String?
    var sunrise: Int?
    var sunset: Int?
    
    init(json: JSON?) {
        self.type = json?["type"] as? Int
        self.id = json?["id"] as? Int
        self.message = json?["message"] as? Double
        self.country = json?["country"] as? String
        self.sunrise = json?["sunrise"] as? Int
        self.sunset = json?["sunset"] as? Int
    }
    
    func toJson() -> JSON? {
        return nil
    }
}

