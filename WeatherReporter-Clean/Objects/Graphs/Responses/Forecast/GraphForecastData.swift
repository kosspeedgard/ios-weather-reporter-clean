//
//  GraphForecastData.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastData: GraphDecoder {
    var dateTime: Int?
    var main: GraphForecastMain?
    var weather: [GraphForecastWeather]?
    var clouds: GraphForecastClouds?
    var wind: GraphForecastWind?
    var rain: GraphForecastRain?
    var system: GraphForecastSystem?
    var dateTimeText: String?
    
    init(json: JSON?) {
        weather = [GraphForecastWeather]()
        self.dateTime = json?["dt"] as? Int
        self.main = GraphForecastMain(json: json?["main"] as? JSON)
        self.clouds = GraphForecastClouds(json: json?["clouds"] as? JSON)
        self.wind = GraphForecastWind(json: json?["wind"] as? JSON)
        self.rain = GraphForecastRain(json: json?["rain"] as? JSON)
        self.system = GraphForecastSystem(json: json?["sys"] as? JSON)
        self.dateTimeText = json?["dt_txt"] as? String
        
        if let _weather = json?["weather"] as? [JSON] {
            for json in _weather {
                let weather = GraphForecastWeather(json: json)
                self.weather?.append(weather)
            }
        }
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
