//
//  GraphForecastSystem.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastSystem: GraphDecoder {
    var pod: String?
    
    init(json: JSON?) {
        self.pod = json?["pod"] as? String
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
