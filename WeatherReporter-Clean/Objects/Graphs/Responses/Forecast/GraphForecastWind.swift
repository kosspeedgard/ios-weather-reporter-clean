//
//  GraphForecastWind.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastWind: GraphDecoder {
    var speed: Double?
    var deg: Double?
    
    init(json: JSON?) {
        self.speed = json?["speed"] as? Double
        self.deg = json?["deg"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}

