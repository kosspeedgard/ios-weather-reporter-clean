//
//  GraphForecastMain.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastMain: GraphDecoder {
    var temperature: Double?
    var tempMin: Double?
    var tempMax: Double?
    var pressure: Double?
    var seaLevel: Double?
    var groundLvel: Double?
    var humidity: Double?
    var tempKF: Double?
    
    init(json: JSON?) {
        self.temperature = json?["temp"] as? Double
        self.tempMin = json?["temp_min"] as? Double
        self.tempMax = json?["temp_max"] as? Double
        self.pressure = json?["pressure"] as? Double
        self.seaLevel = json?["sea_level"] as? Double
        self.groundLvel = json?["grnd_level"] as? Double
        self.humidity = json?["humidity"] as? Double
        self.tempKF = json?["temp_kf"] as? Double
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
