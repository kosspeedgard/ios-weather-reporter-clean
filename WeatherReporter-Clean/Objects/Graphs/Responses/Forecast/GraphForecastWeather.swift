//
//  GraphForecastWeather.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastWeather: GraphDecoder {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
    
    init(json: JSON?) {
        self.id = json?["id"] as? Int
        self.main = json?["main"] as? String
        self.description = json?["description"] as? String
        self.icon = json?["icon"] as? String
    }
    
    func toJson() -> JSON? {
        return nil
    }
}

