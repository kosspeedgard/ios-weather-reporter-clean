//
//  GraphForecastBase.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastBase: GraphDecoder {
    var code: Int?
    var message: Double?
    var count: Int?
    var forecasts: [GraphForecastData]?
    var city: GraphForecastCity?
    
    init(json: JSON?) {
        forecasts = [GraphForecastData]()
        self.code = json?["cod"] as? Int
        self.message = json?["message"] as? Double
        self.count = json?["cnt"] as? Int
        self.city = GraphForecastCity(json: json?["city"] as? JSON)
        
        if let _forecasts = json?["list"] as? [JSON] {
            for json in _forecasts {
                let forecast = GraphForecastData(json: json)
                self.forecasts?.append(forecast)
            }
        }
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
