//
//  GraphForecastClouds.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphForecastClouds: GraphDecoder {
    var all: Int?
    
    init(json: JSON?) {
        self.all = json?["all"] as? Int
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
