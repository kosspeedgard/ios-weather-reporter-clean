//
//  GraphError.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct GraphError: GraphDecoder {
    var code: Int?
    var message: String?
    var error: String?
    
    init() {}
    
    init(json: JSON?) {
        self.code = json?["code"] as? Int
        self.message = json?["message"] as? String
    }
    
    func toJson() -> JSON? {
        return nil
    }
}
