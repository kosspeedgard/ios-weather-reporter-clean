//
//  AppDisplayable.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 15/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import UIKit

protocol AppDisplayable {
    func display(title: String, message: String, action: ((UIAlertAction) -> Void)?)
}

extension AppDisplayable where Self: UIViewController {
    func display(title: String, message: String, action: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alertController.addAction(
            UIAlertAction(title: "OK", style: .default, handler: action)
        )
        
        guard let rootController = UIApplication.shared.keyWindow?.rootViewController else { return }
        rootController.present(alertController, animated: true, completion: nil)
    }
}
