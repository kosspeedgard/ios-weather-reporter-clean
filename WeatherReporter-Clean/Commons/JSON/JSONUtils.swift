//
//  JSONUtils.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class JSONUtils: NSObject {
    static func convertJsonToString(json: JSON) -> String? {
        do {
            let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [])
            let jsonString = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue) as String?
            
            return jsonString
        }
    }
    
    static func dataToJson(data: Data) -> JSON? {
        do {
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as String?
            let dictionary = convertStringToJson(string: jsonString!)
            
            return dictionary
        }
    }
    
    static func convertStringToJson(string: String) -> JSON? {
        if let data = string.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
