//
//  Utils.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class Utils: NSObject {
    static func timeFromUtils(time: Int?, format: String) -> String? {
        return Date(unix: time ?? 0).string(format: format)
    }
    
    static func covertToCelsius(kelvin: Double) -> Int {
        let celsius = kelvin - 273.15
        return Int(celsius)
    }
    
    static func convertToFarenheit(kelvin: Double) -> Int {
        let farenheit = 9 / 5 * (kelvin - 273.15) + 32
        return Int(farenheit)
    }
}
