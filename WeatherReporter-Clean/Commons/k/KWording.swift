//
//  KWording.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

//MARK: Network
let kSelfNetworkError = "Network cannot use now."
let kDataNotFound = "Data not found."

//MARK: Weather
let kToF = "To °F"
let kToC = "To °C"
