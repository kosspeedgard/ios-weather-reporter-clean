//
//  ErrorUtils.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class ErrorUtils: NSObject {
    static func localError(message: String, code: Int) -> Error {
        let userInfo = [NSLocalizedDescriptionKey: message]
        return NSError(domain: "", code: code, userInfo: userInfo)
    }
}
