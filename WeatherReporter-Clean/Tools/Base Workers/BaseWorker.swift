//
//  BaseWorker.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class BaseWorker: NSObject {
    func sendRequestWith<T: GraphDecoder>(method: HTTPMethods, host: Config.Host, urlSuffixs: String, json: JSON?, responseType: T.Type, completion: @escaping(_ graph: T?) -> Void, failure: @escaping(_ error: GraphError?) -> Void) {
        let url = host.rawValue + urlSuffixs + Config.APIConfig.key
        
        Network.shared.requestWith(method: method, url: url, json: json, completion: { [weak self] (data) in
            let graph = self?.decoder(data: data, responseType: responseType)
            completion(graph)
        }, failure: { [weak self] (error) in
            self?.detectError(error: error, completion: { (graphError) in
                failure(graphError)
            })
        })
    }
    
    //MARK: Decoder
    private func decoder<T: GraphDecoder>(data: Data?, responseType: T.Type) -> T? {
        guard let data = data else { return nil }
        
        let json = JSONUtils.dataToJson(data: data)
        let decoded = responseType.init(json: json)
        
        return decoded
    }
    
    //MARK: Handle Error
    private func handleError(with json: JSON) -> GraphError? {
        let error = GraphError(json: json)
        return error
    }
    
    private func handleLocalError(message: String?, code: Int) -> GraphError? {
        var error = GraphError()
        error.message = message
        error.code = code
        error.error = "Local Error"
        
        return error
    }
    
    private func detectError(error: NetworkError, completion: @escaping(_ error: GraphError?) -> Void) {
        switch error {
        case .customError(let description):
            completion(handleLocalError(message: description, code: 0))
        case .networkFailure:
            completion(handleLocalError(message: kSelfNetworkError, code: 1))
        case .statusCodeNotOK(let data):
            completion(decoder(data: data, responseType: GraphError.self))
        }
    }
}
