//
//  Network.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

class Network: NSObject {
    static let shared: Network = Network()
    
    private override init() {}
    
    func requestWith(method: HTTPMethods, url: String, json: JSON? = nil, completion: @escaping(_ data: Data?) -> Void, failure: @escaping(_ error: NetworkError) -> Void) {
        let _url = URL(string: url)!
        
        var urlRequest = URLRequest(url: _url)
        urlRequest.httpMethod = method.rawValue
        
        if let json = json {
            urlRequest.httpBody = JSONUtils.convertJsonToString(json: json)?.data(using: .utf8)
        }
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10.0
        configuration.timeoutIntervalForResource = 10.0
        
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        let dataTask = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            if error != nil {
                let description = "Send \(method) is error: \(String(describing: error?.localizedDescription))"
                taskMain {
                    failure(NetworkError.customError(description: description))
                }
                
                return
            }
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                switch statusCode {
                case 200...299:
                    taskMain {
                        if let data = data {
                            completion(data)
                        }
                        else {
                            failure(NetworkError.customError(description: kDataNotFound))
                        }
                    }
                    
                    break
                default:
                    taskMain {
                        failure(NetworkError.statusCodeNotOK(data: data))
                    }
                    
                    break
                }
            }
        })
        
        dataTask.resume()
    }
}

//MARK: URLSessionDelegate
extension Network: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(.useCredential, urlCredential)
    }
}
