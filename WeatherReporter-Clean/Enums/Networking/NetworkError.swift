//
//  NetworkError.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case customError(description: String)
    case networkFailure
    case statusCodeNotOK(data: Data?)
}
