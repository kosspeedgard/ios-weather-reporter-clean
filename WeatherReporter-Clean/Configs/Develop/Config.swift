//
//  Config.swift
//  WeatherReporter-Clean
//
//  Created by lifeup on 14/1/2562 BE.
//  Copyright © 2562 Khwan Siricharoenporn. All rights reserved.
//

import Foundation

struct Config {
    //MARK: Host
    enum Host: String {
        case baseURL = "https://api.openweathermap.org/"
    }
    
    //MARK: APIConfig
    struct APIConfig {
        static let key = "&APPID=f4f7e9f29295684d802b275ad6ca6d5c"
    }
}
